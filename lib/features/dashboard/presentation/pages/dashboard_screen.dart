import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:movie_app_final/core/constants/app_constants.dart';
import 'package:movie_app_final/core/constants/theme_constants.dart';
import 'package:movie_app_final/core/utils/utils.dart';
import 'package:movie_app_final/features/dashboard/presentation/controllers/dashboard_controller.dart';
import 'package:movie_app_final/features/dashboard/presentation/widgets/category_widget.dart';

class DashboardHomeScreen extends StatelessWidget {
  DashboardHomeScreen({Key? key}) : super(key: key);
  final DashboardController _dashboardController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeConstants.backgroundColor,
      body: Padding(
        padding: AppConstants.screenMainPadding(),
        child: Obx((){
          return _dashboardController.isLoading.value ?
            const Center(child: CircularProgressIndicator(),) :
            SingleChildScrollView(
              child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Utils.getSizedBox(height: 40),
                const CustomBannerImage(),
                Utils.getSizedBox(height: 30),

                Row(
                  children: [
                    Utils.getText("Top Rated Movies",style: const TextStyle(fontFamily: "PoppinsRegular",fontSize: 18,color: ThemeConstants.textMainColor)),
                    const Spacer(),
                    InkWell(
                        onTap: (){
                          _dashboardController.navigateToSeeAllScreen(appBarTitle: "Top Rated Movies",isUpcomingMoviesCategory: false);
                        },
                        child: Utils.getText("See all",style: const TextStyle(fontFamily: "PoppinsRegular",fontSize: 14,color: ThemeConstants.hyperlinkColor))
                    ),
                  ],
                ),
                Utils.getSizedBox(height: 10),

                SizedBox(
                  height: 180,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: _dashboardController.topRatedModel.results.length<5 ? _dashboardController.topRatedModel.results.length : 5,
                    itemBuilder: (context,index){
                      return Padding(
                        padding: const EdgeInsets.only(right: 16),
                        child: CategoryWidget(imageUrl: _dashboardController.topRatedModel.results[index].backdropPath, movieName: _dashboardController.topRatedModel.results[index].title, vote: _dashboardController.topRatedModel.results[index].voteAverage),
                      );
                    },
                  )
                ),

                Utils.getSizedBox(height:20),
                Row(
                  children: [
                    Utils.getText("Upcoming Movies",style: const TextStyle(fontFamily: "PoppinsRegular",fontSize: 18,color: ThemeConstants.textMainColor)),
                    const Spacer(),
                    InkWell(
                        onTap: (){
                          _dashboardController.navigateToSeeAllScreen(appBarTitle: "Upcoming Movies",isUpcomingMoviesCategory: true);
                        },
                        child: Utils.getText("See all",style: const TextStyle(fontFamily: "PoppinsRegular",fontSize: 14,color: ThemeConstants.hyperlinkColor))
                    ),
                  ],
                ),
                Utils.getSizedBox(height: 10),

                SizedBox(
                    height: 180,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: _dashboardController.upcomingModel.results.length < 5 ? _dashboardController.upcomingModel.results.length : 5,
                      itemBuilder: (context,index){
                        return Padding(
                          padding: const EdgeInsets.only(right: 16),
                          child: CategoryWidget(imageUrl: _dashboardController.upcomingModel.results[index].backdropPath, movieName: _dashboardController.upcomingModel.results[index].title, vote: _dashboardController.upcomingModel.results[index].voteAverage),
                        );
                      },
                    )
                ),
                Utils.getSizedBox(height:20),

                Row(
                  children: [
                    Utils.getText("Trending Movies",style: const TextStyle(fontFamily: "PoppinsRegular",fontSize: 18,color: ThemeConstants.textMainColor)),
                    const Spacer(),
                    InkWell(
                        onTap: (){
                          _dashboardController.navigateToSeeAllScreen(appBarTitle: "Upcoming Movies",isUpcomingMoviesCategory: true);
                        },
                        child: Utils.getText("See all",style: const TextStyle(fontFamily: "PoppinsRegular",fontSize: 14,color: ThemeConstants.hyperlinkColor))
                    ),
                  ],
                ),
                Utils.getSizedBox(height: 10),
                SizedBox(
                    height: 180,
                    child: ListView.builder(
                      reverse: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: _dashboardController.topRatedModel.results.length<5 ? _dashboardController.topRatedModel.results.length : 5,
                      itemBuilder: (context,index){
                        return Padding(
                          padding: const EdgeInsets.only(right: 16),
                          child: CategoryWidget(imageUrl: _dashboardController.topRatedModel.results[index].backdropPath, movieName: _dashboardController.topRatedModel.results[index].title, vote: _dashboardController.topRatedModel.results[index].voteAverage),
                        );
                      },
                    )
                ),

              ],
            ),
          );
        }),
      ),
    );
  }
}


//Static images are used for now
class CustomBannerImage extends StatelessWidget {
  const CustomBannerImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
          height: 170.0,
          disableCenter: true,
          viewportFraction: 1,
          autoPlay: true
      ),
      items: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 3),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset("assets/images/banner1.jpg",fit: BoxFit.fill,)
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset("assets/images/banner2.jpg",fit: BoxFit.fill,)
          ),
        ),
      ],
    );
  }
}
