import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:movie_app_final/core/constants/app_constants.dart';
import 'package:movie_app_final/core/constants/theme_constants.dart';
import 'package:movie_app_final/core/utils/utils.dart';
import 'package:movie_app_final/features/dashboard/presentation/controllers/dashboard_controller.dart';
import 'package:movie_app_final/features/dashboard/presentation/widgets/category_widget.dart';


class SeeAllScreen extends StatelessWidget {

  final String appBarTitle;
  final bool isUpcomingMoviesCategory;
  final DashboardController _dashboardController = Get.put(DashboardController());
  SeeAllScreen({Key? key,required this.appBarTitle,required this.isUpcomingMoviesCategory}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeConstants.backgroundColor,
      appBar: AppBar(
        backgroundColor: ThemeConstants.backgroundColor,
        centerTitle: true,
        title: Utils.getText(appBarTitle),
      ),
      body: GridView.builder(
          padding: AppConstants.screenMainPadding(),
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,mainAxisSpacing: 30,crossAxisSpacing: 20,),
          itemCount: isUpcomingMoviesCategory ?_dashboardController.upcomingModel.results.length : _dashboardController.topRatedModel.results.length,
          itemBuilder: (context,index){
            return
              isUpcomingMoviesCategory ?
              CategoryWidget(
              imageUrl: _dashboardController.upcomingModel.results[index].backdropPath,
              movieName: _dashboardController.upcomingModel.results[index].title,
              vote: _dashboardController.upcomingModel.results[index].voteAverage,
              isForSeeAllScreen:true,

              ) :
              CategoryWidget(
                imageUrl: _dashboardController.topRatedModel.results[index].backdropPath,
                movieName: _dashboardController.topRatedModel.results[index].title,
                vote: _dashboardController.topRatedModel.results[index].voteAverage,
                isForSeeAllScreen:true,
              );
          }
      ),
    );
  }
}
