import 'package:get/get.dart';
import 'package:movie_app_final/features/dashboard/data/models/toprated_movie_model.dart';
import 'package:movie_app_final/features/dashboard/data/models/upcoming_movie_model.dart';
import 'package:movie_app_final/features/dashboard/domain/use_cases/get_top_rated_movies.dart';
import 'package:movie_app_final/features/dashboard/domain/use_cases/get_upcoming_movies.dart';
import 'package:movie_app_final/features/dashboard/presentation/pages/see_all_screen.dart';


class DashboardController extends GetxController{

  final GetTopRatedMovies _getTopRatedMovies = Get.find();
  final GetUpcomingMovies _getUpcomingMovies = Get.find();
  RxBool isLoading = false.obs;

  late TopRatedModel topRatedModel;
  late UpcomingModel upcomingModel;

  //This will fetch both the category data and controls the loading state to make the 2 api call look like one api call..
  fetchAllData()async{
    isLoading.value = true;
    await fetchTopRatedMovies();
    await fetchUpcomingMovies();
    isLoading.value = false;
  }

  fetchTopRatedMovies()async{
    var response = await _getTopRatedMovies.call();
    response.fold(
            (l) => l.showErrorMessage(),
            (r) => topRatedModel = r
    );
  }

  fetchUpcomingMovies()async{
    var response = await _getUpcomingMovies.call();
    response.fold(
            (l) => l.showErrorMessage(),
            (r) => upcomingModel = r
    );
  }

  //Depending upon the second parameter the see_all screen will fetch the data from either _getTopRatedMovies or _getUpcomingMovies of dashboard controller..
  navigateToSeeAllScreen({required String appBarTitle, required bool isUpcomingMoviesCategory}){
    Get.to(SeeAllScreen( appBarTitle: appBarTitle,isUpcomingMoviesCategory: isUpcomingMoviesCategory,));
  }

  @override
  void onInit() {
    super.onInit();
    fetchAllData();
  }

}