import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:movie_app_final/core/constants/theme_constants.dart';
import 'package:movie_app_final/core/utils/utils.dart';

class CategoryWidget extends StatelessWidget {
  final String imageUrl;
  final String movieName;
  final double vote;
  bool? isForSeeAllScreen;
  CategoryWidget({Key? key, required this.imageUrl,this.isForSeeAllScreen, required this.movieName, required this.vote}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  SizedBox(
      width: Get.width*0.3,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: isForSeeAllScreen == null ?
              Image.network("https://image.tmdb.org/t/p/w185/" + imageUrl,height: 145,fit: BoxFit.fill,) :
              Image.network("https://image.tmdb.org/t/p/w185/" + imageUrl,height: 140,fit: BoxFit.fill,)
          ),
          Utils.getSizedBox(height: 5),
          Utils.getText(movieName,style: const TextStyle(fontFamily: "PoppinsRegular",fontSize: 14,overflow: TextOverflow.ellipsis,color: ThemeConstants.textMainColor,),maxLines: 1),
        ],
      ),
    );
  }
}
