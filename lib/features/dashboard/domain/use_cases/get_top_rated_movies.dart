import 'package:dartz/dartz.dart';
import 'package:get/get.dart';
import 'package:movie_app_final/core/failures/failure.dart';
import 'package:movie_app_final/core/use_cases/use_case.dart';
import 'package:movie_app_final/features/dashboard/data/models/toprated_movie_model.dart';
import 'package:movie_app_final/features/dashboard/data/repository/dashboard_repository_impl.dart';
import 'package:movie_app_final/features/dashboard/domain/repository/dashboard_repository.dart';


class GetTopRatedMovies implements UseCases<TopRatedModel>{

  final DashboardRepository dashboardRepository = Get.find();

  @override
  Future<Either<Failures,TopRatedModel>> call()async{
      return await dashboardRepository.getTopRatedMovies();
  }

}