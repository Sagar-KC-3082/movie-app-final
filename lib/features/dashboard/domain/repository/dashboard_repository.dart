import 'package:dartz/dartz.dart';
import 'package:movie_app_final/core/failures/failure.dart';
import 'package:movie_app_final/features/dashboard/data/models/toprated_movie_model.dart';
import 'package:movie_app_final/features/dashboard/data/models/upcoming_movie_model.dart';


abstract class DashboardRepository {
  Future<Either<Failures,TopRatedModel>> getTopRatedMovies();
  Future<Either<Failures,UpcomingModel>> getUpcomingMovies();
}