import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:movie_app_final/core/constants/network_constants.dart';
import 'package:movie_app_final/core/failures/failure.dart';
import 'package:movie_app_final/features/dashboard/data/models/toprated_movie_model.dart';
import 'package:movie_app_final/features/dashboard/data/models/upcoming_movie_model.dart';


abstract class DashboardRemoteDataSource {
  Future<UpcomingModel> getUpcomingMovies();
  Future<TopRatedModel> getTopRatedMovies();
}

class DashboardRemoteDataSourceImpl implements DashboardRemoteDataSource{

  final Dio dio = Get.find();

  @override
  Future<UpcomingModel> getUpcomingMovies()async{
    var response = await dio.get(NetworkConstants.upComingMoviesApi);
    if(response.statusCode==200){
      return UpcomingModel.fromJson(response.data);
    }
    else{
      throw ServerFailure();
    }
  }

  @override
  Future<TopRatedModel> getTopRatedMovies()async{
    var response = await dio.get(NetworkConstants.topRatedMoviesApi);
    if(response.statusCode==200){
      return TopRatedModel.fromJson(response.data);
    }
    else{
      throw ServerFailure();
    }

  }


}

