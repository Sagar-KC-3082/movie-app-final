import 'package:connectivity/connectivity.dart';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';
import 'package:movie_app_final/core/failures/failure.dart';
import 'package:movie_app_final/features/dashboard/data/data_sources/dashboard_remote_source.dart';
import 'package:movie_app_final/features/dashboard/data/models/toprated_movie_model.dart';
import 'package:movie_app_final/features/dashboard/data/models/upcoming_movie_model.dart';
import 'package:movie_app_final/features/dashboard/domain/repository/dashboard_repository.dart';

class DashboardRepositoryImpl implements DashboardRepository{

  final DashboardRemoteDataSource dashboardRemoteDataSource = Get.find();
  // Connectivity connectivity = Get.find();

  @override
  Future<Either<Failures,TopRatedModel>> getTopRatedMovies()async{


    try{
      final data = await dashboardRemoteDataSource.getTopRatedMovies();
      return Right(data);
    }
    catch(e){
      return Left(ServerFailure());
    }

    // if(await connectivity.checkConnectivity() == ConnectivityResult.none){
    //   return Left(NetworkFailure());
    // }
    // else{
    //   try{
    //     final data = await dashboardRemoteDataSource.getTopRatedMovies();
    //     return Right(data);
    //   }
    //   catch(e){
    //     return Left(ServerFailure());
    //   }
    // }


  }

  @override
  Future<Either<Failures,UpcomingModel>> getUpcomingMovies()async{

    var connectionResult = await (Connectivity().checkConnectivity());
    if( connectionResult == ConnectivityResult.none){
      return Left(NetworkFailure());
    }
    else{
      try{
        final data = await dashboardRemoteDataSource.getUpcomingMovies();
        return Right(data);
      }
      catch(e){
        return Left(ServerFailure());
      }
    }
  }

}