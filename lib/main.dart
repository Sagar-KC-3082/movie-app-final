import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:movie_app_final/core/bindings/custom_binder.dart';
import 'features/dashboard/presentation/pages/dashboard_screen.dart';


void main()async{
  CustomBindings().dependencies();
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent
  ));
  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialBinding: CustomBindings(),
      home: DashboardHomeScreen(),
    )
  );
}



