abstract class Failures {
  showErrorMessage();
}


class ServerFailure implements Failures{
  @override showErrorMessage() {
    return "Server is not working";
  }
}

class NetworkFailure implements Failures {
  @override showErrorMessage() {
    return "Internet is not working";
  }
}