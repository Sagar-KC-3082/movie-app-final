import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:movie_app_final/core/use_cases/use_case.dart';
import 'package:movie_app_final/features/dashboard/data/data_sources/dashboard_remote_source.dart';
import 'package:movie_app_final/features/dashboard/data/repository/dashboard_repository_impl.dart';
import 'package:movie_app_final/features/dashboard/domain/repository/dashboard_repository.dart';
import 'package:movie_app_final/features/dashboard/domain/use_cases/get_top_rated_movies.dart';
import 'package:movie_app_final/features/dashboard/domain/use_cases/get_upcoming_movies.dart';
import 'package:movie_app_final/features/dashboard/presentation/controllers/dashboard_controller.dart';


class CustomBindings implements Bindings{

  @override
  void dependencies() {
    Get.lazyPut<Dio>(() => Dio());
    // Get.put(Connectivity());
    Get.lazyPut<GetTopRatedMovies>(() => GetTopRatedMovies());
    Get.lazyPut<GetUpcomingMovies>(() => GetUpcomingMovies());
    Get.lazyPut<DashboardRemoteDataSource>(() => DashboardRemoteDataSourceImpl());
    Get.lazyPut<DashboardRepository>(() => DashboardRepositoryImpl());
    Get.lazyPut<DashboardController>(() => DashboardController());
  }

}