import 'package:flutter/cupertino.dart';

class AppConstants {

  static const double textScaleFactor = 1;

  static EdgeInsets screenMainPadding(){
    return const EdgeInsets.symmetric(horizontal: 20,vertical: 10);
  }

}