import 'package:flutter/material.dart';

class ThemeConstants {

  static const backgroundColor = Colors.black;
  static const textMainColor = Colors.white;
  static const hyperlinkColor = Colors.red;

}