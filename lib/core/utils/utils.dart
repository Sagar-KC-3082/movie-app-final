import 'package:flutter/cupertino.dart';
import 'package:movie_app_final/core/constants/app_constants.dart';


class Utils {

  static SizedBox getSizedBox({double? height, double? width}){
    return SizedBox(height: height,width: width,);
  }

  static Text getText(String text,{TextStyle? style, int? maxLines}){
    return Text(text,style: style,textScaleFactor: AppConstants.textScaleFactor,maxLines:maxLines ?? 1 ,);
}

}