import 'package:dartz/dartz.dart';
import 'package:movie_app_final/core/failures/failure.dart';


abstract class UseCases<Type> {
  Future<Either<Failures,Type>>call();
}